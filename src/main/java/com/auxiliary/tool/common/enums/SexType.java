package com.auxiliary.tool.common.enums;

/**
 * <p>
 * <b>文件名：SexType.java</b>
 * </p>
 * <p>
 * <b>用途：</b>定义性别枚举
 * </p>
 * <p>
 * <b>编码时间：2023年10月8日 上午8:37:37
 * </p>
 * <p>
 * <b>修改时间：2023年10月8日 上午8:37:37
 * </p>
 *
 *
 * @author 彭宇琦
 * @version Ver1.0
 * @since JDK 1.8
 * @since autest 4.3.0
 */
public enum SexType {
    /**
     * 男
     */
    MAN,
    /**
     * 女
     */
    WOMEN;
}
